﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public AudioClip Sound;
    public float Speed;

    private Rigidbody2D Rigidbody2D;
    private Vector2 Direction;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        Camera.main.GetComponent<AudioSource>().PlayOneShot(Sound);    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody2D.velocity = Direction * Speed;
    }
    public void SetDirection(Vector2 direction)
    {
        Direction = direction;
    }

    public void destroyBullet()
    {
        Destroy(gameObject);
    }

   // private void OnCollisionEnter2D(Collision2D collision)
 //   {
     //   Juanmovimiento juan = collision.collider.GetComponent<Juanmovimiento>();
    //    jaimemov jaime = collision.collider.GetComponent<jaimemov>();
   //     if (juan != null)
   //     {
   //         juan.Hit();
   //     }
   //     if (jaime != null)
  //      {
   //         jaime.Hit();
   //     }
 //       destroyBullet();
  //  }
}
